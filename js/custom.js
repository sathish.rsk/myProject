
$(function(){
	function PmediaContent(data , idCus){
	$(idCus).empty();
	for(var i = 0 ; i < data.length ; i++){
		var left = '<div class="col-lg-6 col-12 px-5 pt-2"><div class="card border-0 bg-transparent text-dark"><div class="card-body"><h2 class="card-title py-2"><span class="text-DecCusG">'+data[i].head+'</span></h2><p class="card-text">'+data[i].content+'</p><a href="#" ><span class="text-primary"><i class="fas fa-envelope px-3"></i></span> Contact US</a></div></div></div>';
		var right = '<div class="col-lg-6 col-12 px-0"><img src="image/product/p-Cusimg/'+parseInt(i+1)+'.jpg" class="img-fluid"></div>';
		var template = "";
		if(i % 2 == 0){
			template = '<div class="row font-weight-normal" id = "">'+left+right +'</div>';
		}else{
			template = '<div class="row font-weight-normal" id = "">'+right+left+'</div>';
		}
		$(idCus).append(template);
	}
	preLoad();
}
	console.log("Welcome !!!!");
	//FireBase Configuration
    var config = {
        apiKey: "AIzaSyBjFKlaE2L_6PPypwiu2xDVk1Qqhmo0Crk",
        authDomain: "rsk-7647.firebaseapp.com",
        databaseURL: "https://rsk-7647.firebaseio.com/",
        projectId: "rsk-7647",
        storageBucket: "",
        messagingSenderId: "773362054153"
    };
    firebase.initializeApp(config);

    //FireBase Reference In my File
    const myDbRef = firebase.database();
    
    //Include Header And Footer
	$("#addHeader").load("header.html"); 
	$("#addFooter").load("footer.html");

	//Display the first Tab on load
	$('#myTab li:first-child a').tab('show');
	
	//Count Function on  Employee
	var n = 0;
	setTimeout(countDown,10);
	function countDown(){
	   n+=1;
	   if(n < 55){
	      setTimeout(countDown,10);
	   }
	   $('#empCount span').text(n);
	}
	preLoad();
	function preLoad(){
		$('#loadModal').modal('toggle');
	}
	//Hover function on navbar
	$(document).on({
	    mouseenter: function () {
	        $(this).toggleClass("show").find(".dropdown-menu").toggleClass("show");
	    },
	    mouseleave: function () {
	       $(this).toggleClass("show").find(".dropdown-menu").toggleClass("show");
	    }
	} , '#cusMenu ul li');

	//underLine function in what function
	$('.whatIcon .card-deck .card').hover(function(){
		$(this).find("h5").children().toggleClass("text-DecCus");
	});

	//Coursel Interval Custom
	$('.carousel').carousel({
	  interval: 3000
	});

	//Link functionality on navbar to define Tabs
	//navbar
	function pdtList(link){
		$('#myTab a[href="#'+link+'"]').tab('show');
		pdtTabHelp(link);
	}
	
	//Help function on content display
	function pdtTabHelp(link){
		preLoad();
		"#"+link == "#market" ? myDbRef.ref('marketData/').once('value').then(function(data) {
		      	PmediaContent( data.val() , '#market');
		    }) : "";
		"#"+link == "#environment" ? myDbRef.ref('environmentData/').once('value').then(function(data) {
		      PmediaContent( data.val() , '#environment');
		    }) : "";
		"#"+link == "#framework" ? myDbRef.ref('frameworkData/').once('value').then(function(data) {
		      PmediaContent( data.val() , '#framework');
		    }) : "";
	}
	
	//nav link click on page define
	$(document).on('click','#pdtList a',function(){
		var link = $(this).attr("cusData");
		pdtList(link);
	});

	//default call funtionality
	myDbRef.ref('marketData/').once('value').then(function(data) {
      PmediaContent( data.val() , '#market');
    });
	/*myDbRef.ref('marketData/').once('value' , data => console.log(data.val()));*/
	
	//My tab Contents
	$(document).on('click','#myTab li', function(){
		pdtTabHelp($(this).find('.active').attr("aria-controls"));
	});
});
