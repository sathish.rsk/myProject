var left = '<div class="col-6 px-5 pt-2"><div class="card border-0 bg-transparent text-dark"><div class="card-body"><h2 class="card-title py-2"><span class="text-DecCusG">Media Project Management</span></h2><p class="card-text">Acheron Media Project Management (MPM) is a comprehensive solution that is built on OpenText Media Management (OTMM) to empower the marketing teams and project manager to manage all their creative workflow in one place - from initial request to delivery. It automates the Digital Media Project life cycle and delivers a holistic approach to the digital MPM, extending OTMM and Customer Experience Management with the intelligent BPM.</p><a href="#" ><span class="text-primary"><i class="fas fa-envelope px-3"></i></span> Contact US</a></div></div></div>';
var right = '<div class="col-6 px-0"><img src="image/product/p-Cusimg/1.jpg" class="img-fluid"></div>';
var template = '<div class="row font-weight-normal" id = "">'+left+right +'</div>';


/*	var marketData = [
		{
			"head": "Media Project Management",
		 "content":"Acheron Media Project Management (MPM) is a comprehensive solution that is built on OpenText Media Management (OTMM) to empower the marketing teams and project manager to manage all their creative workflow in one place - from initial request to delivery. It automates the Digital Media Project life cycle and delivers a holistic approach to the digital MPM, extending OTMM and Customer Experience Management with the intelligent BPM."
		},
		{
			"head": "Request Project Management",
		 "content":"Acheron Request Management (RM) is a comprehensive solution that is built on OpenText Media Management (OTMM) to manage all the creative requests in one place, from initial request to delivery. Solution provides the Creative teams to get the right to work with requests that come in a common format in the unified experience and with the right context including supporting documents and other artifacts to support immediate action."
		},
		{
			"head":"Translations Workflow",
			"content": "Translation workflow is a complete solution for systemizing and automating the Packaging and Marketing Approval workflows using Open Text Products. It automates the Process flows, Data Models and System security, integrating it with OpenText Business Process Management (OTBPM)."
		},
		{
			"head" : "21 CFR Part 11 Compliant Solution",
			"content" : "Acheron “Part11” solution framework supports life science organizations achieve compliance with the requirements of Rule 21, Code of Federal Regulations, Part 11 to manage all the marketing digital assets and documents. Framework is built on OpenText Media Management and OpenText Process Suite Platform, which enables life science organization to comply with 21 CFR Part 11."
		},
		{
			"head" : "PAM",
			"content" : "Acheron PAM (Purchase Order Approval Management) application for SAP is a software solution built on top of Open Text Process Suite to manage the end to end workflow of Purchase Request, Approval and Order Creation. It integrates at once, the Buyer / Purchase Requests, Approver / Approval Process and the Vendor / Order Creation, to stream line the logistics by saving time of the material to be purchased. It is integrated with SAP all through the process."
		},
		{
			"head" : "Mobile Solutions – Appworks",
			"content" : "To provide greater benefits to the customers who are using our solutions, we have extended our existing applications via AppWorks Gateway. This delivers faster value to the business users, accelerating the Approval process. This solution is currently available for Apple iOS and Google Android and provide access to the device hardware and mobile operating system."
		}		
	];
	var environmentData = [
		{
			"head": "EHS Package",
		 	"content":"Acheron EHSMS (Environment, Health and Safety Management System) solution is built on Open Text Process suite, Content Server and Brava Viewer. It leverages the Open Text Process platform core engine to design, build, deploy and manage the business processes. This solution is built on top of Entity Modelling to capture the EHS entities, its associated tasks and its life cycle. On the business perspective, this solution will set the business to set for the future by."
		},
		{
			"head": "Action Management",
		 	"content":"Action Management Module is a reusable Module which can run on its own. It provides support component for other modules. It has its own life cycle management; Actions can be registered from external modules like Corrective Action Report, Audit, Safety Topic Scheduler, etc., Action will have Action Owner, Verifier and a Sign off person. System would provide an experience for the user to Perform, Review and Complete the action."
		},
		{
			"head":"Incident Management",
			"content": "Incident Management Module is the one which would act as the supporting component for other modules. During the lifecycle of EHS operations, the Incidents will be documented and stored in this module. This Module is connected to Open Text Content server or Archive server at the backend. The documents can be reviewed and annotated using Brava viewer."
		},
		{
			"head" : "Safety Imaging Package",
			"content" : "Safety Image Package Module allows the user to schedule, track and report any EHS training topics. User can setup a Safety topic and assign a creator and facilitator. Creator can upload the Safety topic materials and send for review and approval. Facilitator conducts the training and marks the topic as complete. The outcome of the Training are captured as Actions in the “Action Module”"
		},
		{
			"head" : "Corrective Action Reports",
			"content" : "Corrective Action Reports (CAR) can be triggered from Audit Module or it can be registered and triggered as a stand alone module. It is an integral part of an organization’s, continuous improvement plan. Solution enables the knowledge workers to capture, track and maintain accurate records. System allows to assign tasks, follow-up corrective action tasks to employees and track those tasks. It also allows the user to register the actions from CAR."
		},
		{
			"head" : "Audit Management",
			"content" : "Audit Management module manages the end-to- end lifecycle of the Audit. The solution allows and makes it easy for the knowledge workers to manage any type of Audits, Activities, Tasks, Associated data and processes from EHS, to Quality and to Conformance. System provides pro-active notification and alerts to the Auditee, Lead Auditor and Management Representative. Audit module is deeply integrated with the Corrective Action Reporting Module."
		}		
	];
	var frameworkData = [
		{
			"head": "Process Suite Accelerator",
		 	"content":"Acheron PSA is a Framework built with a responsive Process portal which is integrated with,It delivers the power and flexibility to digitize, automate and integrate process across functions, systems, machines and clouds. This innovative platform supports tight integration between content and process to connect the right person, system or thing with the content it needs at the right time."
		},
		{
			"head": "User Provisioning",
		 	"content":"Acheron OTMM User Provisioning (AOUP) is a framework built on OpenText Process Suite technology by leveraging the Entity Modeling architecture and “Information-first” development methodology to provision and onboard users in OpenText Media Management (OTMM). AOUP allows organizations to automate the OTMM User registration process such as account creation, editing, deleting, automatically assign OTMM user security policy rights."
		},
		{
			"head":"Digitize Docs",
			"content": "Tired of managing the information through paper based format. Here is the solution – Acheron eDocs, that we have built to convert your physical document (PDF) to Entities / Digital intelligent applications. Remember, these are not a replacement of scanning OCC. Just upload your PDF documents and the rest is ensured, as it turns into huge database of information that can be used for Management purposes – Audit, Marketing, etc., Undoubtedly, this is heavily packed with several other advantages like Security, Easy retrieval, Version Control and Sharing."
		},
		{
			"head" : "Safety Imaging Package",
			"content" : "Safety Image Package Module allows the user to schedule, track and report any EHS training topics. User can setup a Safety topic and assign a creator and facilitator. Creator can upload the Safety topic materials and send for review and approval. Facilitator conducts the training and marks the topic as complete. The outcome of the Training are captured as Actions in the “Action Module”"
		},
		{
			"head" : "Corrective Action Reports",
			"content" : "Corrective Action Reports (CAR) can be triggered from Audit Module or it can be registered and triggered as a stand alone module. It is an integral part of an organization’s, continuous improvement plan. Solution enables the knowledge workers to capture, track and maintain accurate records. System allows to assign tasks, follow-up corrective action tasks to employees and track those tasks. It also allows the user to register the actions from CAR."
		},
		{
			"head" : "Audit Management",
			"content" : "Audit Management module manages the end-to- end lifecycle of the Audit. The solution allows and makes it easy for the knowledge workers to manage any type of Audits, Activities, Tasks, Associated data and processes from EHS, to Quality and to Conformance. System provides pro-active notification and alerts to the Auditee, Lead Auditor and Management Representative. Audit module is deeply integrated with the Corrective Action Reporting Module."
		}		
	];*/